﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events_Handler : MonoBehaviour
{
    [SerializeField]
    RectTransform events_panel;

    public GameObject eventBoxPrefab;
    
    float drag_offsetValue;
    float panel_orientation_offset_value;

    void Start()
    {
        AdjustPanelDimensions();
        drag_offsetValue = 0;
        panel_orientation_offset_value = 0;
    }

    private void AdjustPanelDimensions()
    {
        CustomFunc.RescaleCanvasToScreenResolution(); // change canvas target resolution to match phone screen dimensions
        Vector2 events_PanelCalculatedDimensions;

        // calculates size of individual box depending on cound of events panel's child and screen size
        if (Screen.orientation == ScreenOrientation.Portrait)
            events_PanelCalculatedDimensions = new Vector2(0, panel_orientation_offset_value +  Screen.height - (Screen.height / 8.0f) * events_panel.transform.childCount);
        else
            events_PanelCalculatedDimensions = new Vector2(0, panel_orientation_offset_value +  Screen.height - (Screen.height / 4.0f) * events_panel.transform.childCount);

        // pass the calculated size to actual panel dimensions
        events_panel.offsetMin = events_PanelCalculatedDimensions;

        // adjust offset so panel can move without distortion
        events_panel.offsetMax = new Vector2(0 , panel_orientation_offset_value);


        panel_orientation_offset_value += drag_offsetValue*0.5f;
    }

    public void TEST_INPUT_EVENT()
    {
        string dataString = GameObject.Find("Data Handler").GetComponent<DataHandlerScript>().GetDataString();

        string[] dataStringRows = SplitDataIntoRows(dataString);

        GameObject[] notRequiredFields = GameObject.FindGameObjectsWithTag("NotRequired"); 
        foreach(GameObject field in notRequiredFields)
        {
            Destroy(field);
        }

        for (int i=0;i<dataStringRows.Length-1;i++)
        {
            string[] dataFields = SplitRowIntoFields(dataStringRows[i]);
           // int eventID = int.Parse(dataFields[0]);
            string eventName = dataFields[1];
            string eventStartDate = dataFields[2];
            string eventEndDate = dataFields[3];
            string eventVenue = dataFields[4];
            Instantiate_EventBox(null, eventName, "Start: " + eventStartDate + " | End: " + eventEndDate, eventVenue);
        }

    }

    string[] SplitDataIntoRows(string data)
    {
        string[] dataRows = data.Split(';');
        return dataRows;
    }

    string[] SplitRowIntoFields(string row)
    {
        string[] dataFields = row.Split('|');
        return dataFields;
    }

    public void Instantiate_EventBox(Sprite icon, string event_name, string event_schedule, string event_venue)
    {
        

        eventBoxPrefab.GetComponent<InputBox>().setEventIcon(icon);
        eventBoxPrefab.GetComponent<InputBox>().setEventName(event_name);
        eventBoxPrefab.GetComponent<InputBox>().setEventSchedule(event_schedule);
        eventBoxPrefab.GetComponent<InputBox>().setEventVenue(event_venue);
        eventBoxPrefab.GetComponent<InputBox>().setPrefabParent(events_panel.transform);

        Instantiate(eventBoxPrefab, Vector3.zero, Quaternion.identity);
    }

    //DRAG AND UPDATES
    private void Update()
    {
        // ADD CONDITION IF PANEL EVENT IS OPEn
        AdjustPanelDimensions(); //always refit scales to adjust screen

        if (Input.touchCount > 0)
        {
            drag_offsetValue += Input.GetTouch(0).deltaPosition.y;
        }
        
        drag_offsetValue = Mathf.Lerp(drag_offsetValue, 0.0f, 0.5f);
    }
}
