﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputBox : MonoBehaviour
{
    [SerializeField]
    private Image eventIcon;
    [SerializeField]
    private Text eventName;
    [SerializeField]
    private Text eventSchedule;
    [SerializeField]
    private Text eventVenue;
    [SerializeField]
    private Transform prefab_parent;


    public void setEventIcon (Sprite image)
    {
        eventIcon.sprite = image;
    }
    public void setEventName(string input)
    {
        eventName.text = input;
    }
    public void setEventSchedule(string input)
    {
        eventSchedule.text = input;
    }
    public void setEventVenue(string input)
    {
        eventVenue.text = input;
    }

    //parent setting
    private void Start()
    {
        transform.SetParent(prefab_parent);
    }
    public void setPrefabParent(Transform target)
    {
        prefab_parent = target;
    }
}
