﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomFunc 
{
    public static bool BOOL_CheckIfAppHasInternet()
    {
        switch (Application.internetReachability)
        {
            case NetworkReachability.NotReachable:
                return false;

            default:
                return true;
        }
    }

    public static void RescaleCanvasToScreenResolution()
    {
        GameObject.Find("Canvas").GetComponent<CanvasScaler>().referenceResolution = new Vector2 (Screen.width , Screen.height);
    }
}
