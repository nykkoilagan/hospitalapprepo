﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataHandlerScript : MonoBehaviour
{
    string eventsDataReturnString;

    private void Start()
    {
        StartCoroutine(GetData());
    }

    // Start is called before the first frame update
    IEnumerator GetData()
    {
        UnityWebRequest eventsData = UnityWebRequest.Get("http://localhost/HospitalCorporationApp/eventsData.php");
        yield return eventsData.SendWebRequest();

        if(eventsData.isNetworkError || eventsData.isHttpError)
        {
            Debug.Log(eventsData.error);
        }
        else
        {
            //show results
            Debug.Log(eventsData.downloadHandler.text);
        }

        eventsDataReturnString = eventsData.downloadHandler.text;
    }

    public string GetDataString()
    {
        StartCoroutine(GetData());
        Debug.Log("Data GetData Called");

        return eventsDataReturnString;
    }
    
}
